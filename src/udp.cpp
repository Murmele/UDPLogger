/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "udp.h"

#ifdef SIMULATE_UDP
#include <QHostAddress>
#include "TestUdpSocket.h"
#else
#include <QUdpSocket>
#endif

#include <iostream>
#include <array>
#include <QtDebug>
#include <QMutex>
#include <QFile>
#include <QTimer>
#include <sys/time.h>

#include <qcustomplot.h>
#include "plotbuffer.h"
#include "signals.h"

#include "exportdata.h"

UDP::UDP(QMutex* mutex, PlotBuffers* data_buffers, Signals* signal,
         const TriggerSettings& settings)
    : m_mutex(mutex), m_data_buffers(data_buffers), m_signals(signal),
      m_trigger_settings(settings) {
  m_socket = new UdpSocket(this);
  connect(m_socket, &UdpSocket::readyRead, this, &UDP::readData);
  m_timer = new QTimer(this);

  connect(m_timer, &QTimer::timeout, this, &UDP::refreshPlot);
}
bool UDP::init() {
  return init(QHostAddress::AnyIPv4, 60000, 200, 10, 1, "", "");
}

bool UDP::init(const QHostAddress& hostaddress, quint16 port,
               int udp_buffer_size, int refresh_rate, int use_data_count,
               const QString& export_path, const QString& project_name) {

  m_socket->disconnectFromHost();
  m_use_data_count = use_data_count;
  m_if_file_ready = false;
  m_udp_buffer_size = udp_buffer_size;
  m_udp_buffer.resize(udp_buffer_size);
  m_udp_index = 0;
  m_export_path = export_path;
  m_project_name = project_name;
  m_data_changed = false;
  m_trigger_in_progress = false;

  m_buffer_smaller_than_message = 0;

  m_actual_index = 0;
  m_udp_global_index = 0;

  if (!m_socket->bind(hostaddress, port)) {
    emit showInfoMessageBox(
        tr("Not able to open UDP"),
        QString(tr("Hostaddress or Port not valid: '%1'\nMaybe a second "
                   "instance of UDPLogger is open?"))
            .arg(m_socket->errorString()));
    std::cout << "Bind: NOK" << std::endl;
    return 0;
  }

  m_timer->start(
      1000 /
      refresh_rate); // calling refreshPlot after every timeout of the timer

  std::cout << "Bind: OK" << std::endl;

  return 1;
}

void UDP::refreshPlot() {
  if (m_data_changed) {
    m_data_changed = false;
    emit dataChanged();
    emit newTriggerValue(m_actual_value);
  }
}

void UDP::readData() {

  m_time_difference = calculateTimedifference();

  if (m_actual_index >= m_use_data_count) {
    m_actual_index = 0;
  }

  if (m_udp_index >= m_udp_buffer_size) {
    m_udp_index = 0;
  }
  struct udpMessageBuffer puffer;
  qint64 size = m_socket->readDatagram(puffer.puffer, UDP_CONSTANTS::max_data,
                                       nullptr, nullptr);

  if (size > UDP_CONSTANTS::max_data &&
      m_buffer_smaller_than_message == 0) { // only one time
    m_buffer_smaller_than_message = 1;
    emit showInfoMessageBox(
        tr("UDP message size greater than max data"),
        tr("The received UDP message size (") + QString::number(size) +
            tr(" Byte) is greater than the maximum allowed message size of ") +
            QString::number(UDP_CONSTANTS::max_data) + " Byte." +
            tr("This means, that not every signal can be plotted"));
  }

  // save data in udp buffer
  m_mutex->lock(); // brauchts das locken überhaupt?
  m_udp_buffer[m_udp_index] = puffer;

  // store data in data buffer which is the buffer for the plot
  // only every m_m_use_data_count data should be plottet
  if (m_ifread_data && m_actual_index == 0) {
    // Attention, m_m_use_data_count and m_redrawcount problem
    // Add data to data buffer, so the diagrams will be refreshed
    m_data_buffers->addData(puffer.puffer, UDP_CONSTANTS::max_data);
    m_data_changed = true;
  }

  m_mutex->unlock();

  if (m_signals->getSignalCount() > 0) {
    if (m_trigger_settings.signalIndex >= m_signals->getSignalCount())
      return;
    Signal triggerSignal = m_signals->getSignal(m_trigger_settings.signalIndex);
    m_actual_value = m_data_buffers->getValue(
        puffer.puffer, UDP_CONSTANTS::max_data, triggerSignal);
  }
  // Trigger
  if (m_trigger_settings.enabled && !m_trigger_in_progress) {
    bool triggered = false;
    if (m_actual_value > m_trigger_settings.level &&
        m_actual_value > m_previous_value &&
        (m_trigger_settings.type == TriggerType::RISING_EDGE ||
         m_trigger_settings.type == TriggerType::ALL_EDGES)) {
      triggered = true;
    } else if (m_actual_value < m_trigger_settings.level &&
               m_actual_value < m_previous_value &&
               (m_trigger_settings.type == TriggerType::FALLING_EDGE ||
                m_trigger_settings.type == TriggerType::ALL_EDGES)) {
      triggered = true;
    } else {
      m_previous_value = m_actual_value;
    }

    if (triggered) {
      startTrigger();
    }
  }

  m_actual_index++;
  m_udp_index++;
  m_udp_global_index++;
}

void UDP::connectDataReady() { m_ifread_data = 1; }

void UDP::disconnectDataReady() { m_ifread_data = 0; }

int64_t UDP::calculateTimedifference() {
  struct timeval time;
  gettimeofday(&time, nullptr);
  int64_t actual_time = time.tv_sec * 1e6 + time.tv_usec;
  int64_t difference = actual_time - m_timeLastDataReceived;

  m_timeLastDataReceived = actual_time;

  return difference;
}

struct udpMessageBuffer UDP::getValueIndexBefore(int index) const {
  int index_new = m_udp_index - index;
  if (index_new < 0) {
    index_new = m_udp_buffer_size - index_new;
  }

  return m_udp_buffer[index_new];
}

void UDP::timerTimeout() {

  emit triggerFinished();

  double time_after = m_trigger_settings.timeAfterTrigger;
  double time_before = m_trigger_settings.timeBeforeTrigger;

  // Calculate the first udp buffer index which shall be stored. This must be
  // done because the index might lie in the past of the trigger time (if
  // time_before > 0)
  int first_index;
  if (std::abs(time_after) <= 0.000001) {
    if (std::abs(time_before) <= 0.000001) {
      emit disableTrigger();
      emit showInfoMessageBox(
          QObject::tr("Trigger times invalid"),
          QObject::tr("Please set 'Time before trigger' or 'Time after "
                      "trigger' unequal to zero"));
      exportFinished();
      return;
    }
    // mean number of messages during "time_before" because the first index is
    // not known when the trigger happens
    const auto indices_time_before =
        static_cast<int>(time_before) / m_time_difference;
    first_index = m_trigger_index - indices_time_before;
  } else {
    // can be assumed, that index_before don't get an overflow because it is a
    // 64bit variable
    // (m_udp_global_index - m_trigger_index)/time_after: number of indices per
    // time unit.
    first_index = m_trigger_index -
                  static_cast<int>(time_before / time_after *
                                   (m_udp_global_index - m_trigger_index));
  }

  // index_before can also be negative, so it is possible to just subtract
  if (m_udp_buffer_size - (m_udp_global_index - first_index) < 0) {
    emit showInfoMessageBox(
        QObject::tr("Bufferoverflow of UDP buffer"),
        QObject::tr("With the actual trigger time settings, "
                    "the number of values which should be stored (") +
            QString::number(m_udp_global_index - first_index) + ") " +
            tr("is higher "
               "than the UDP buffer size (") +
            QString::number(m_udp_buffer_size) +
            tr("). So the data repeats. Please set "
               "the trigger times lower or set the UDP buffersize to a higher "
               "value"));

    exportFinished();
    return;
  }

  m_export =
      new ExportData(m_export_path, m_project_name, m_udp_buffer, first_index,
                     m_udp_global_index, m_udp_buffer_size, m_signals, this);
  connect(m_export, &ExportData::resultReady, this, &UDP::exportFinished);
  connect(m_export, &ExportData::showInfoMessage, this,
          &UDP::showInfoMessageBox);
  m_export->run();
}

void UDP::exportFinished() {
  emit triggerFinished();
  if (m_export) {
    m_export->deleteLater();
    m_export = nullptr;
  }
  m_trigger_in_progress = false;
}

void UDP::startTrigger() {
  emit triggerStarted();
  QTimer::singleShot(m_trigger_settings.timeAfterTrigger * 1000, this,
                     &UDP::timerTimeout);
  m_trigger_in_progress = true;
  if (m_trigger_settings.signalIndex >= m_signals->getSignalCount())
    return;
  Signal triggerSignal = m_signals->getSignal(m_trigger_settings.signalIndex);
  m_previous_value = m_data_buffers->getValue(
      m_udp_buffer[m_udp_index].puffer, UDP_CONSTANTS::max_data, triggerSignal);
  m_trigger_index = m_udp_global_index;
}

void UDP::setTriggerSettings(const TriggerSettings settings) {
  m_trigger_settings = settings;
}

void UDP::setDataChanged(bool changed) { m_data_changed = changed; }

UDP::~UDP() { delete m_socket; }
