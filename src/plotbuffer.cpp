#include "plotbuffer.h"

PlotBuffer::PlotBuffer() {}

PlotBuffer::PlotBuffer(int index_x, int index_y)
    : m_index_x(index_x), m_index_y(index_y) {
  m_count = 0;
}

int PlotBuffer::decreaseCounter() {
  m_count--;
  return m_count;
}

PlotBuffers::PlotBuffers(Signals* signal)
    : QObject(nullptr), m_signals(signal) {}

PlotBuffers::~PlotBuffers() {}

void PlotBuffers::setPlotBufferSize(int plot_buffer_size) {
  m_plot_buffer_size = plot_buffer_size;
}

QSharedPointer<QCPGraphDataContainer>
PlotBuffers::getBuffer(const Signal& xaxis, const Signal& yaxis) {

  // Check if a plotbuffer for this combination of x and y already exist
  for (int i = 0; i < m_plot_buffer.length(); i++) {
    auto index_x = m_plot_buffer[i]->getIndexX();
    auto index_y = m_plot_buffer[i]->getIndexY();

    if (index_x == xaxis.index && index_y == yaxis.index) {
      m_plot_buffer[i]->increaseCounter();
      return m_plot_buffer[i];
    }
  }

  // Create new buffer because no buffer exists yet
  QSharedPointer<PlotBuffer> buffer =
      QSharedPointer<PlotBuffer>(new PlotBuffer(xaxis.index, yaxis.index));
  m_plot_buffer.append(buffer);
  m_plot_buffer[m_plot_buffer.length() - 1]->increaseCounter();
  return m_plot_buffer[m_plot_buffer.length() - 1];
}

// decreases the counter of the buffer which holds pointer_old
void PlotBuffers::removeSignal(const Signal& xaxis, const Signal& yaxis) {
  for (int i = 0; i < m_plot_buffer.length(); i++) {
    int index_x = m_plot_buffer[i]->getIndexX();
    int index_y = m_plot_buffer[i]->getIndexY();
    if (index_x == xaxis.index && index_y == yaxis.index) {
      if (m_plot_buffer[i]->decreaseCounter() <= 0) {
        m_plot_buffer.removeAt(i);
      }
      return;
    }
  }
}

void PlotBuffers::removeGraph(const Signal& xaxis, const Signal& yaxis) {
  for (int i = 0; i < m_plot_buffer.length(); i++) {
    auto index_x = m_plot_buffer[i]->getIndexX();
    auto index_y = m_plot_buffer[i]->getIndexY();

    if (index_x == xaxis.index && index_y == yaxis.index) {
      int counter = m_plot_buffer[i]->decreaseCounter();
      if (counter <= 0) { // should never be that the counter decreases below 0
        m_plot_buffer.removeAt(i);
      }
    }
  }
}

// TODO: might be slow!!!!
double PlotBuffers::getValue(const char* data, int length,
                             const Signal& signal) {

  if (signal.datatype.compare("float") == 0) {
    if (length > signal.startByte + 4 - 1) // preventing pufferoverflow
      return static_cast<double>(getFloat(signal.startByte, data));
  } else if (signal.datatype.compare("double") == 0) {
    if (length > signal.startByte + 8 - 1)
      return getDouble(signal.startByte, data);
  } else if (signal.datatype.compare("int8_t") == 0) {
    if (length > signal.startByte)
      return static_cast<double>(getChar(signal.startByte, data));
  } else if (signal.datatype.compare("uint8_t") == 0) {
    if (length > signal.startByte)
      return static_cast<double>(getUChar(signal.startByte, data));
  } else if (signal.datatype.compare("int16_t") == 0) {
    if (length > signal.startByte + 2 - 1)
      return static_cast<double>(getShort(signal.startByte, data));
  } else if (signal.datatype.compare("uint16_t") == 0) {
    if (length > signal.startByte + 2 - 1)
      return static_cast<double>(getUShort(signal.startByte, data));
  } else if (signal.datatype.compare("bool") == 0) {
    if (length > signal.startByte)
      return static_cast<double>(getBool(signal.startByte, data));
  } else if (signal.datatype.compare("int32_t") == 0) {
    if (length > signal.startByte + 4 - 1)
      return static_cast<double>(getInt(signal.startByte, data));
  } else if (signal.datatype.compare("uint32_t") == 0) {
    if (length > signal.startByte + 4 - 1)
      return static_cast<double>(getUInt(signal.startByte, data));
  } else if (signal.datatype.compare("uint64_t") == 0) {
    if (length > signal.startByte + 8 - 1)
      return static_cast<double>(getUInt64(signal.startByte, data));
  } else if (signal.datatype.compare("int64_t") == 0) {
    if (length > signal.startByte + 8 - 1)
      return static_cast<double>(getInt64(signal.startByte, data));
  }
  return 0; // not defined
}

void PlotBuffers::addData(const char* data, int length) {
  for (int i = 0; i < m_plot_buffer.length(); i++) {
    int index_xaxis = m_plot_buffer[i]->getIndexX();
    int index_yaxis = m_plot_buffer[i]->getIndexY();

    const double xValue =
        getValue(data, length, m_signals->getSignal(index_xaxis));
    const double yValue =
        getValue(data, length, m_signals->getSignal(index_yaxis));

    // QCPGraphDataContainer data;
    // data.add(new_data);
    // auto value = data.at(0);
    // data.remove(value->key);
    // m_plot_buffer = new QVector<PlotBuffer>;
    QVector<QCPGraphData> new_data = {QCPGraphData(xValue, yValue)};
    m_plot_buffer[i]->add(new_data, true);

    for (int j = 0; j < m_plot_buffer[i]->size() - m_plot_buffer_size; j++) {
      auto value_at_index = m_plot_buffer[i]->at(0);
      m_plot_buffer[i]->remove(value_at_index->key);
    }
  }
}

void PlotBuffers::clearPlots() {
  for (int i = 0; i < m_plot_buffer.length(); i++) {
    m_plot_buffer[i].data()->clear();
  }
}
