/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "changegraphdialog.h"
#include "ui_changegraphdialog.h"
#include "qcustomplot.h"
#include "plot.h"
#include "signals.h"

ChangeGraphDialog::ChangeGraphDialog(const Signals* signal,
                                     const PlotSettings& settings,
                                     QWidget* parent)
    : QDialog(parent), ui(new Ui::changeGraphDialog), m_signals(signal) {
  ui->setupUi(this);

  ui->combo_color->addItem(tr("blue"), static_cast<int>(Qt::blue));
  ui->combo_color->addItem(tr("black"), static_cast<int>(Qt::black));
  ui->combo_color->addItem(tr("red"), static_cast<int>(Qt::red));
  ui->combo_color->addItem(tr("darkred"), static_cast<int>(Qt::darkRed));
  ui->combo_color->addItem(tr("green"), static_cast<int>(Qt::green));
  ui->combo_color->addItem(tr("darkBlue"), static_cast<int>(Qt::darkBlue));
  ui->combo_color->addItem(tr("cyan"), static_cast<int>(Qt::cyan));
  ui->combo_color->addItem(tr("magenta"), static_cast<int>(Qt::magenta));
  ui->combo_color->addItem(tr("yellow"), static_cast<int>(Qt::yellow));
  ui->combo_color->addItem(tr("darkYellow"), static_cast<int>(Qt::darkYellow));
  ui->combo_color->addItem(tr("gray"), static_cast<int>(Qt::gray));
  ui->combo_color->addItem(tr("darkGray"), static_cast<int>(Qt::darkGray));

  ui->combo_scatter_style->addItem(tr("None"), QCPScatterStyle::ssNone);
  ui->combo_scatter_style->addItem(tr("Dot"), QCPScatterStyle::ssDot);
  ui->combo_scatter_style->addItem(tr("Cross"), QCPScatterStyle::ssCross);
  ui->combo_scatter_style->addItem(tr("Plus"), QCPScatterStyle::ssPlus);
  ui->combo_scatter_style->addItem(tr("Circle"), QCPScatterStyle::ssCircle);
  ui->combo_scatter_style->addItem(tr("Disc"), QCPScatterStyle::ssDisc);
  ui->combo_scatter_style->addItem(tr("Square"), QCPScatterStyle::ssSquare);
  ui->combo_scatter_style->addItem(tr("Diamond"), QCPScatterStyle::ssDiamond);
  ui->combo_scatter_style->addItem(tr("Star"), QCPScatterStyle::ssStar);
  ui->combo_scatter_style->addItem(tr("Triangle"), QCPScatterStyle::ssTriangle);
  ui->combo_scatter_style->addItem(tr("TriangleInverted"),
                                   QCPScatterStyle::ssTriangleInverted);
  ui->combo_scatter_style->addItem(tr("CrossSquare"),
                                   QCPScatterStyle::ssCrossSquare);
  ui->combo_scatter_style->addItem(tr("PlusSquare"),
                                   QCPScatterStyle::ssPlusSquare);

  ui->combo_linestyle->addItem(tr("None"), QCPGraph::LineStyle::lsNone);
  ui->combo_linestyle->addItem(tr("Line"), QCPGraph::LineStyle::lsLine);
  ui->combo_linestyle->addItem(tr("StepLeft"), QCPGraph::LineStyle::lsStepLeft);
  ui->combo_linestyle->addItem(tr("StepRight"),
                               QCPGraph::LineStyle::lsStepRight);
  ui->combo_linestyle->addItem(tr("StepCenter"),
                               QCPGraph::LineStyle::lsStepCenter);
  ui->combo_linestyle->addItem(tr("Impulse"), QCPGraph::LineStyle::lsImpulse);

  ui->spinbox_y_max->setRange(-4294967296, 4294967296);
  ui->spinbox_y_max->setDecimals(3);
  ui->spinbox_y_min->setRange(-4294967296, 4294967296);
  ui->spinbox_y_min->setDecimals(3);

  ui->spinbox_range_adjustment->setRange(0, 4294967296);

  updateSignals();
  setSettings(settings);

  connect(ui->listWidget, &QListWidget::currentRowChanged, this,
          &ChangeGraphDialog::listWidgetRowChanged);
  connect(ui->txt_name, &QTextEdit::textChanged, this,
          &ChangeGraphDialog::updateData2);
  connect(ui->pb_cancel, &QPushButton::clicked, this,
          &ChangeGraphDialog::cancel);
  connect(ui->pb_apply, &QPushButton::clicked, this, &ChangeGraphDialog::apply);
  connect(ui->pb_ok, &QPushButton::clicked, this, &ChangeGraphDialog::ok);
  connect(ui->pb_delete, &QPushButton::clicked, this,
          &ChangeGraphDialog::deleteElement);
  connect(ui->pb_add, &QPushButton::clicked, this,
          qOverload<>(&ChangeGraphDialog::addElement));
}

void ChangeGraphDialog::disableSignalSettings(bool disable) {
  ui->combo_color->setEnabled(!disable);
  ui->combo_linestyle->setEnabled(!disable);
  ui->combo_scatter_style->setEnabled(!disable);
  ui->txt_name->setEnabled(!disable);
  ui->combo_signalname_yaxis->setEnabled(!disable);
  ui->combo_signalname_xaxis->setEnabled(!disable);
}

void ChangeGraphDialog::listWidgetRowChanged(int row) {

  if (row < 0) {
    return;
  }
  if (m_previous_row >= 0) {
    // Store set data in the settings, because the widgets are reused now for
    // another signal
    m_settings.signal_settings[m_previous_row].color = QColor(
        static_cast<Qt::GlobalColor>(ui->combo_color->currentData().toInt()));
    m_settings.signal_settings[m_previous_row].linestyle =
        static_cast<QCPGraph::LineStyle>(
            ui->combo_linestyle->currentData().toInt());
    m_settings.signal_settings[m_previous_row].scatterstyle =
        static_cast<QCPScatterStyle::ScatterShape>(
            ui->combo_scatter_style->currentData().toInt());
    m_settings.signal_settings[m_previous_row].name =
        ui->txt_name->toPlainText();
    m_settings.signal_settings[m_previous_row].signal_yaxis =
        m_signals->getSignal(ui->combo_signalname_yaxis->currentData().toInt());
  }
  m_previous_row = row;

  // set data for new selected signal
  for (int i = 0; i < ui->combo_color->count(); i++) {
    if (QColor(static_cast<Qt::GlobalColor>(
            ui->combo_color->itemData(i).toInt())) ==
        m_settings.signal_settings.at(row).color) {
      ui->combo_color->setCurrentIndex(i);
      break;
    }
  }

  for (int i = 0; i < ui->combo_linestyle->count(); i++) {
    if (ui->combo_linestyle->itemData(i).value<int>() ==
        m_settings.signal_settings.at(row).linestyle) {
      ui->combo_linestyle->setCurrentIndex(i);
      break;
    }
  }

  for (int i = 0; i < ui->combo_scatter_style->count(); i++) {
    if (ui->combo_scatter_style->itemData(i).value<int>() ==
        m_settings.signal_settings.at(row).scatterstyle) {
      ui->combo_scatter_style->setCurrentIndex(i);
      break;
    }
  }

  for (int i = 0; i < ui->combo_signalname_yaxis->count(); i++) {
    if (m_signals->getSignal(i).name.compare(
            m_settings.signal_settings.at(row).signal_yaxis.name) == 0) {
      ui->combo_signalname_yaxis->setCurrentIndex(i);
      break;
    }
  }

  ui->txt_name->setText(m_settings.signal_settings.at(row).name);
}

void ChangeGraphDialog::updateData(const QString& text) {
  int row = ui->listWidget->currentRow();

  if (row > 0) {
    m_settings.signal_settings[row].linestyle =
        static_cast<QCPGraph::LineStyle>(
            ui->combo_linestyle->currentData().toInt());
    m_settings.signal_settings[row].scatterstyle =
        static_cast<QCPScatterStyle::ScatterShape>(
            ui->combo_scatter_style->currentData().toInt());
    m_settings.signal_settings[row].color =
        ui->combo_color->currentData().value<QColor>();
  }
}

void ChangeGraphDialog::updateData2() {
  int row = ui->listWidget->currentRow();

  if (row >= 0) {
    m_settings.signal_settings[row].name = ui->txt_name->toPlainText();
    ui->listWidget->item(row)->setText(ui->txt_name->toPlainText());
  }
}

void ChangeGraphDialog::cancel() { close(); }

void ChangeGraphDialog::apply() {
  // remove deleted graphs!!!

  int row = ui->listWidget->currentRow();
  if (row >= 0) {
    m_settings.signal_settings[row].color = QColor(
        static_cast<Qt::GlobalColor>(ui->combo_color->currentData().toInt()));
    m_settings.signal_settings[row].linestyle =
        static_cast<QCPGraph::LineStyle>(
            ui->combo_linestyle->currentData().toInt());
    m_settings.signal_settings[row].scatterstyle =
        static_cast<QCPScatterStyle::ScatterShape>(
            ui->combo_scatter_style->currentData().toInt());
    m_settings.signal_settings[row].name = ui->txt_name->toPlainText();
    m_settings.signal_settings[row].signal_xaxis =
        m_signals->getSignal(ui->combo_signalname_xaxis->currentData().toInt());
    m_settings.signal_settings[row].signal_yaxis =
        m_signals->getSignal(ui->combo_signalname_yaxis->currentData().toInt());
    m_settings.ymin = ui->spinbox_y_min->value();
    m_settings.ymax = ui->spinbox_y_max->value();
    m_settings.automatic_value = ui->spinbox_range_adjustment->value();
    m_settings.ifautomatic_range = ui->checkbox_if_automatic_range->isChecked();
    m_settings.ifrelative_ranging = ui->rb_relative->isChecked();
  }
  emit settingsChanged(m_settings);
}

void ChangeGraphDialog::ok() {
  apply();
  close();
}

// at the moment directly deleted, not possible to get back
void ChangeGraphDialog::deleteElement() {

  int row = ui->listWidget->currentRow();
  QListWidgetItem* item =
      ui->listWidget->takeItem(row); // removes the element from the list and
                                     // returns a pointer to the item to delete
  m_previous_row = -1;
  delete item;
  m_settings.signal_settings.remove(row);
  m_settings.signal_settings.remove(row);

  if (ui->listWidget->count() <= 0) {
    disableSignalSettings(true);
  }
}

void ChangeGraphDialog::addElement() { addElement(nullptr); }

void ChangeGraphDialog::addElement(
    const struct SignalSettings* settings_import = nullptr) {
  struct SignalSettings settings;
  QString name = "";

  if (settings_import == nullptr) {
    QString temp_name;
    int i = 0;
    while (name.compare("") == 0) {
      bool name_exist = 0;
      i++;
      temp_name = "Default Name" + QString::number(i);
      for (int j = 0; j < ui->listWidget->count(); j++) {
        if (ui->listWidget->item(j)->text().compare(temp_name) == 0) {
          name_exist = 1;
          break;
        }
      }
      if (!name_exist) {
        name = temp_name;
      }
    }

    settings.linestyle = static_cast<QCPGraph::LineStyle>(
        ui->combo_linestyle->itemData(1).toInt());
    settings.scatterstyle = static_cast<QCPScatterStyle::ScatterShape>(
        ui->combo_scatter_style->itemData(0).toInt());
    settings.color = QColor(
        static_cast<Qt::GlobalColor>(ui->combo_color->itemData(0).toInt()));
    settings.name = name;
    if (m_signals->getSignalCount() == 0) {
      QMessageBox msgBox;
      msgBox.setText(tr("No signals available"));
      msgBox.exec();
      return;
    }
    settings.signal_yaxis =
        m_signals->getSignal(ui->combo_signalname_yaxis->itemData(0).toInt());
    settings.signal_xaxis =
        m_signals->getSignal(ui->combo_signalname_xaxis->itemData(0).toInt());
  } else {
    settings.linestyle = settings_import->linestyle;
    settings.scatterstyle = settings_import->scatterstyle;
    settings.color = settings_import->color;
    settings.name = settings_import->name;
    settings.signal_yaxis = settings_import->signal_yaxis;
    settings.signal_xaxis = settings_import->signal_xaxis;
  }

  m_settings.signal_settings.append(settings);

  ui->listWidget->addItem(settings.name);
  ui->listWidget->setCurrentRow(ui->listWidget->count() - 1);
  ui->combo_signalname_yaxis->setCurrentText(settings.signal_yaxis.name);
  ui->combo_signalname_xaxis->setCurrentText(settings.signal_xaxis.name);

  disableSignalSettings(false);
}

void ChangeGraphDialog::updateSignals() {
  // Add available signals to combobox
  ui->combo_signalname_yaxis->clear();
  ui->combo_signalname_xaxis->clear();
  for (int i = 0; i < m_signals->getSignalCount(); i++) {
    const auto& signal = m_signals->getSignal(i);
    ui->combo_signalname_yaxis->addItem(signal.name, i);
    ui->combo_signalname_xaxis->addItem(signal.name, i);
  }
}

ChangeGraphDialog::~ChangeGraphDialog() { delete ui; }

void ChangeGraphDialog::on_spinbox_y_min_valueChanged(double arg1) {
  m_settings.ymin = arg1;
}

void ChangeGraphDialog::on_spinbox_range_adjustment_valueChanged(double arg1) {
  m_settings.automatic_value = arg1;
  if (m_settings.ifrelative_ranging)
    m_settings.automatic_value /= 100;
}

void ChangeGraphDialog::on_spinbox_y_max_valueChanged(double arg1) {
  m_settings.ymax = arg1;
}

void ChangeGraphDialog::on_checkbox_if_automatic_range_toggled(bool checked) {
  m_settings.ifautomatic_range = checked;
}

void ChangeGraphDialog::on_rb_relative_toggled(bool checked) {
  m_settings.ifrelative_ranging = checked;

  if (!checked)
    m_settings.automatic_value *= 100;
}

void ChangeGraphDialog::setSettings(const PlotSettings& settings) {
  m_settings = settings;

  ui->checkbox_if_automatic_range->setChecked(settings.ifautomatic_range);
  ui->rb_relative->setChecked(settings.ifrelative_ranging);
  ui->rb_absolute->setChecked(!settings.ifrelative_ranging);
  ui->spinbox_y_min->setValue(settings.ymin);
  ui->spinbox_y_max->setValue(settings.ymax);
  ui->spinbox_range_adjustment->setValue(settings.automatic_value);

  if (settings.signal_settings.count() > 0) {
    for (auto& ss : settings.signal_settings)
      ui->listWidget->addItem(ss.name);
    listWidgetRowChanged(0); // select first
  }

  disableSignalSettings(settings.signal_settings.length() <= 0);
}
