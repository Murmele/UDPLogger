#ifndef TESTUDPSOCKET_H
#define TESTUDPSOCKET_H

#include "qhostaddress.h"
#include <QObject>
#include <QHostAddress>

class TestUdpSocket : public QObject {
  Q_OBJECT
public:
  TestUdpSocket(QObject* parent = nullptr);

  void disconnectFromHost(){};
  bool bind(const QHostAddress& hostaddress, quint16 port);
  qint64 readDatagram(char* data, qint64 maxSize,
                      QHostAddress* address = nullptr, quint16* port = nullptr);

  QString errorString() { return m_errorString; }
  void setBindOk(bool ok) { m_bind_ok = ok; }
  void setErrorString(const QString& error) { m_errorString = error; }
  int createUDPPackage();
  void setTime(int32_t);
  void setTemp(float);
  void setGyx(float);
  void setGyy(float);
  void setGyz(float);
signals:
  void readyRead();

private:
  QHostAddress m_hostaddress;
  qint16 m_port;
  bool m_bind_ok{true};
  QString m_errorString;

  char* m_buffer;
  int32_t time;
  float temp;
  float gyx;
  float gyy;
  float gyz;
};

#endif // TESTUDPSOCKET_H
