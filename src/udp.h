/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#ifndef UDP_H
#define UDP_H

#include "triggerwidget.h"
#ifdef SIMULATE_UDP
#define UdpSocket TestUdpSocket
#else
#define UdpSocket QUdpSocket
#endif

#include <array>
#include <QObject>
#include <QVector>

class QFile;
class UdpSocket;
class QMutex;
class QHostAddress;
class PlotBuffers;
class Signals;
class TriggerWidget;
class QTimer;
class QThread;
class ExportData;

struct TriggerSettings;

namespace UDP_CONSTANTS {
const int max_data = 400;
}
struct udpMessageBuffer {
  char puffer[UDP_CONSTANTS::max_data];
};

class UDP : public QObject {
  Q_OBJECT

public:
  UDP(QMutex* mutex, PlotBuffers* data_buffers, Signals* signal,
      const TriggerSettings& settings);
  bool init(const QHostAddress& hostaddress, quint16 port, int udp_buffer_size,
            int refresh_rate, int use_data_count, const QString& export_path,
            const QString& project_name);
  bool init();
  struct udpMessageBuffer getValueIndexBefore(
      int index) const; // returns value "index" before actual m_udp_index
  int64_t calculateTimedifference();
  ~UDP();
public slots:
  void readData();
  void connectDataReady();
  void disconnectDataReady();
  void timerTimeout();
  void exportFinished();
  /*!
   * \brief refreshPlot
   * Triggers a refresh of the plot
   */
  void refreshPlot();
  void startTrigger();
  // Do not use reference here, because this is a queuedConnection
  void setTriggerSettings(const TriggerSettings);

private slots:
  /*! Test function used in the unittests */
  void setDataChanged(bool);

signals:
  void newData();
  void triggerFinished();
  void dataChanged();
  void showInfoMessageBox(QString title, QString text);
  void disableTrigger();
  void newTriggerValue(double value);
  void triggerStarted();

private:
  int m_actual_index{0};
  // Number of udp messages since starup of the application
  // This number will be cleared only after initialization
  int64_t m_udp_global_index{0};

  UdpSocket* m_socket{nullptr};
  // 1 means all incomming messages are processed,
  // 2 means every second message will be processed,
  // 3 means every third message will be processed and so on
  int m_use_data_count{1};
  int m_udp_buffer_size{400};
  bool m_if_file_ready{false};

  QMutex* m_mutex{nullptr};

  bool m_ifread_data{false};
  int m_data_buffer_size;

  bool m_data_changed;

  PlotBuffers* m_data_buffers{nullptr};
  Signals* m_signals{nullptr};
  int m_buffer_smaller_than_message;
  double m_previous_value;
  int64_t m_trigger_index;
  bool m_trigger_in_progress{false};

  QString m_export_path;
  QString m_project_name;

  // ring buffer
  int m_udp_index{0}; // Current index in the udp buffer
  QVector<udpMessageBuffer> m_udp_buffer{
      QVector<udpMessageBuffer>(m_udp_buffer_size)};

  // Last absolute time data was received
  int64_t m_timeLastDataReceived{0};

  QTimer* m_timer{nullptr};

  ExportData* m_export{nullptr};
  int64_t m_time_difference; // [us]

  double m_actual_value{0};

  TriggerSettings m_trigger_settings;

  friend class TestPlots;
  friend class TestUDP;
};

#endif // UDP_H
