/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "mainwindow.h"
#include "settings.h"
#include <QApplication>
#include <unistd.h>
#include <QTranslator>
#include <QDir>
#include <QDebug>

int main(int argc, char* argv[]) {
  QApplication a(argc, argv);

  QCoreApplication::setOrganizationName(PROJECT_NAME);
  QCoreApplication::setOrganizationDomain(PROJECT_HOMEPAGE_URL);
  QCoreApplication::setApplicationName(PROJECT_NAME);

  Settings s;
  const QString lang = s.value("language").toString();

  QTranslator translator;
  if (lang == "system") {
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString& locale : uiLanguages) {
      const QString baseName = QLocale(locale).name();
      if (translator.load(QString(TRANSLATION_FOLDER) + "/udplogger_" +
                          baseName)) {
        a.installTranslator(&translator);
        break;
      }
    }
  } else {
    qDebug() << "Selected language: " << lang;
    if (translator.load(QString(TRANSLATION_FOLDER) + "/udplogger_" + lang)) {
      a.installTranslator(&translator);
    } else
      qDebug() << QString("Language %1 not found").arg(lang);
  }

  QDir translations(TRANSLATION_FOLDER);

  MainWindow w;
  w.show();
  return a.exec();
}
