#include "guisettingsdialog.h"
#include "ui_guisettingsdialog.h"

namespace {
const QHash<QString, QString> languages = {
    {QObject::tr("System"), "system"},    {QObject::tr("Brazilian"), "pt_br"},
    {QObject::tr("Catalan"), "ca"},       {QObject::tr("Czech"), "cs"},
    {QObject::tr("German"), "de"},        {QObject::tr("Danish"), "da"},
    {QObject::tr("Greek"), "el"},         {QObject::tr("English"), "en"},
    {QObject::tr("Spanish"), "es"},       {QObject::tr("French"), "fr"},
    {QObject::tr("Croatian"), "hr"},      {QObject::tr("Italian"), "it"},
    {QObject::tr("Japanese"), "ja"},      {QObject::tr("Polish"), "pl"},
    {QObject::tr("Portuguese"), "pt"},    {QObject::tr("Romanian"), "ro"},
    {QObject::tr("Russian"), "ru"},       {QObject::tr("Sloven"), "sl"},
    {QObject::tr("Dutch"), "nl"},         {QObject::tr("Norwegian"), "nb"},
    {QObject::tr("Dutch_Belgium"), "be"}, {QObject::tr("Hungarian"), "hu"},
    {QObject::tr("Mongolia"), "mn"},
};
}

GuiSettingsDialog::GuiSettingsDialog(const QString& currLang, QWidget* parent)
    : QDialog(parent), ui(new Ui::GuiSettingsDialog) {
  ui->setupUi(this);

  QHashIterator<QString, QString> i(languages);
  int currLangIndex = 0;
  int counter = 0;
  while (i.hasNext()) {
    i.next();
    ui->cb_language->addItem(i.key(), i.value());
    if (i.value() == currLang)
      currLangIndex = counter;
    counter++;
  }
  ui->cb_language->setCurrentIndex(currLangIndex);
}

QString GuiSettingsDialog::language() const {
  const QString lang = ui->cb_language->currentData().toString();
  return lang;
}

GuiSettingsDialog::~GuiSettingsDialog() { delete ui; }
