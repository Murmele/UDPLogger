<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ChangeDiagramProperties</name>
    <message>
        <location filename="../forms/changediagramproperties.ui" line="14"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../forms/changediagramproperties.ui" line="22"/>
        <source>Min. Height</source>
        <translation>Hauteur min.</translation>
    </message>
</context>
<context>
    <name>ChangeGraphDialog</name>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="32"/>
        <source>blue</source>
        <translation>bleu</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="33"/>
        <source>black</source>
        <translation>noir</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="34"/>
        <source>red</source>
        <translation>rouge</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="35"/>
        <source>darkred</source>
        <translation>rouge foncé</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="36"/>
        <source>green</source>
        <translation>vert</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="37"/>
        <source>darkBlue</source>
        <translation>bleu foncé</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="38"/>
        <source>cyan</source>
        <translation>cyan</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="39"/>
        <source>magenta</source>
        <translation>magenta</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="40"/>
        <source>yellow</source>
        <translation>jaune</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="41"/>
        <source>darkYellow</source>
        <translation>jaune foncé</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="42"/>
        <source>gray</source>
        <translation>gris</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="43"/>
        <source>darkGray</source>
        <translation>gris foncé</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="45"/>
        <location filename="../src/changegraphdialog.cpp" line="62"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="46"/>
        <source>Dot</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="47"/>
        <source>Cross</source>
        <translation>Croix</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="48"/>
        <source>Plus</source>
        <translation>Plus</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="49"/>
        <source>Circle</source>
        <translation>Cercle</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="50"/>
        <source>Disc</source>
        <translation>Disque</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="51"/>
        <source>Square</source>
        <translation>Carré</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="52"/>
        <source>Diamond</source>
        <translation>Diamant</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="53"/>
        <source>Star</source>
        <translation>Étoile</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="54"/>
        <source>Triangle</source>
        <translation>Triangle</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="55"/>
        <source>TriangleInverted</source>
        <translation>Triangle inversé</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="57"/>
        <source>CrossSquare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="59"/>
        <source>PlusSquare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="63"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="64"/>
        <source>StepLeft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="65"/>
        <source>StepRight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="67"/>
        <source>StepCenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="69"/>
        <source>Impulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/changegraphdialog.cpp" line="273"/>
        <source>No signals available</source>
        <translation>Aucun signal disponible</translation>
    </message>
</context>
<context>
    <name>ExportData</name>
    <message>
        <location filename="../src/exportdata.cpp" line="57"/>
        <source>No output path!</source>
        <translation>Aucun chemin de sortie !</translation>
    </message>
    <message>
        <location filename="../src/exportdata.cpp" line="58"/>
        <source>Please define export path in the Settings to use the trigger!</source>
        <translation>Veuillez définir le chemin d&apos;export dans les paramètres pour utiliser le déclencheur !</translation>
    </message>
</context>
<context>
    <name>GuiSettingsDialog</name>
    <message>
        <location filename="../forms/guisettingsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../forms/guisettingsdialog.ui" line="22"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../forms/guisettingsdialog.ui" line="34"/>
        <source>Language changes apply after restart.</source>
        <translation>Le changement de langue sera effectif après un redémarrage.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="42"/>
        <source>Store signals to file trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="58"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="60"/>
        <source>&amp;save Settings as ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="61"/>
        <location filename="../src/mainwindow.cpp" line="67"/>
        <location filename="../src/mainwindow.cpp" line="72"/>
        <location filename="../src/mainwindow.cpp" line="90"/>
        <source>Wizard to create a new Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="66"/>
        <source>&amp;save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="71"/>
        <source>&amp;import Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <source>&amp;import Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="78"/>
        <source>Import new Signals from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="84"/>
        <source>Export C/C++ Package function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="89"/>
        <source>&amp;Add Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="94"/>
        <source>Start UDP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="95"/>
        <source>Starts reading from UDP Buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="100"/>
        <source>Stop UDP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="101"/>
        <source>Stops reading from UDP Buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="106"/>
        <source>Clear plot buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="108"/>
        <source>Clearing the plot buffer to plot new data, where the keys are smaller than the last keys.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>Project Settings</source>
        <translation>Paramètres du projet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="117"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="122"/>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="124"/>
        <source>Trigger dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="165"/>
        <location filename="../src/mainwindow.cpp" line="207"/>
        <source>UDP Logger Config Files (*.udpLoggerSettings)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Export Settings</source>
        <translation>Exporter les paramètres</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>Please Stop UDP Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="206"/>
        <source>Open settingsfile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Plot</name>
    <message>
        <location filename="../src/plot.cpp" line="65"/>
        <source>Change Graphstyle</source>
        <translation>Modifier le style de graphique</translation>
    </message>
    <message>
        <location filename="../src/plot.cpp" line="70"/>
        <source>Add Graph</source>
        <translation>Ajouter un graphique</translation>
    </message>
    <message>
        <location filename="../src/plot.cpp" line="74"/>
        <source>Change plot properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plot.cpp" line="78"/>
        <source>Delete Plot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="6"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="7"/>
        <source>Brézilien</source>
        <translation>Brésilien</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="8"/>
        <source>Catalan</source>
        <translation>Catalan</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="9"/>
        <source>Tchèque</source>
        <translation>Tchèque</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="10"/>
        <source>Allemand</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="11"/>
        <source>Danois</source>
        <translation>Danois</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="12"/>
        <source>Grec</source>
        <translation>Grec</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="13"/>
        <source>Anglais</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="14"/>
        <source>Espagnol</source>
        <translation>Espagnol</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="15"/>
        <source>Français</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="16"/>
        <source>Croate</source>
        <translation>Croate</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="17"/>
        <source>Italien</source>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="18"/>
        <source>Japonais</source>
        <translation>Japonais</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="19"/>
        <source>Polonais</source>
        <translation>Polonais</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="20"/>
        <source>Portugais</source>
        <translation>Portugais</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="21"/>
        <source>Roumains</source>
        <translation>Roumain</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="22"/>
        <source>Russe</source>
        <translation>Russe</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="23"/>
        <source>Slovène</source>
        <translation>Slovène</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="24"/>
        <source>Pays-Bas</source>
        <translation>Néerlandais</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="25"/>
        <source>Norvege</source>
        <translation>Norvégien</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="26"/>
        <source>Belgique-Flemish</source>
        <translation>Flamand</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="27"/>
        <source>Hongrois</source>
        <translation>Hongrois</translation>
    </message>
    <message>
        <location filename="../src/guisettingsdialog.cpp" line="28"/>
        <source>Mongol</source>
        <translation>Mongol</translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="218"/>
        <source>Trigger times invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="219"/>
        <source>Please set &apos;Time before trigger&apos; or &apos;Time after trigger&apos; unequal to zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="242"/>
        <source>Bufferoverflow of UDP buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="243"/>
        <source>With the actual trigger time settings, the number of values which should be stored (</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../forms/settingsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="22"/>
        <source>Project Name</source>
        <translation>Nom du projet</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="34"/>
        <source>UDP Connection</source>
        <translation>Connexion UDP</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="44"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="65"/>
        <source>Host address</source>
        <translation>Addresse hôte</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="84"/>
        <source>UDP Buffer Size [Elements]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="108"/>
        <source>Use Element [every X Element]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="127"/>
        <source>Plotting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="135"/>
        <source>Refreshrate [Hz]</source>
        <translation>Taux de rafraîchissement [Hz]</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="156"/>
        <source>Plot Buffer Size [Elements]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="178"/>
        <source>Export Data</source>
        <translation>Exporter les données</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="186"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="196"/>
        <location filename="../forms/settingsdialog.ui" line="226"/>
        <source>Browse File</source>
        <translation>Parcourir les fichiers</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="208"/>
        <source>ExportPackageFunction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="216"/>
        <source>Path for source file</source>
        <translation>Chemin vers le fichier source</translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Relativ path from header file&lt;/p&gt;&lt;p&gt;to declaration file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/settingsdialog.ui" line="251"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Additional includes:&lt;/p&gt;&lt;p&gt;(seperate with &apos;;&apos;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="35"/>
        <source>address</source>
        <translation>adresse</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="36"/>
        <source>AnyIPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="37"/>
        <source>LocalHost</source>
        <translation>Localhost</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="38"/>
        <source>Broadcast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="103"/>
        <location filename="../src/settingsdialog.cpp" line="109"/>
        <source>Set export file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="153"/>
        <source>Data export path does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="164"/>
        <source>ExportSourceFile path is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="176"/>
        <source>Relative header path does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="182"/>
        <source>Acccept settings</source>
        <translation>Accepter les paramètres</translation>
    </message>
</context>
<context>
    <name>Signals</name>
    <message>
        <location filename="../src/signals.cpp" line="72"/>
        <source>Open Signals file</source>
        <translation>Ouvrir le fichier de signaux</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="73"/>
        <source>SignalFiles(%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="125"/>
        <source>Signal %1: Name attribute not found</source>
        <translation>Signal %1 : Attribut name non trouvé</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="137"/>
        <source>Signal %1: Datatype attribute not found</source>
        <translation>Signal %1 : Attribut datatype non trouvé</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="145"/>
        <source>Signal %1: Datatype attribute is invalid</source>
        <translation>Signal %1 : Attribut datatype invalide</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="193"/>
        <source>No description row found</source>
        <translation>Aucune ligne de description trouvée</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="194"/>
        <source>No row found, where in the first column is the text &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="234"/>
        <source>Column not found</source>
        <translation>Colonne non trouvée</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="236"/>
        <source>No column found with the name &apos;%1&apos;. Make sure, that this column name is in the same row as the Index description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="265"/>
        <source>Signal %1: Name is invalid</source>
        <translation>Signal %1 : Attribut name invalide</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="270"/>
        <source>Signal %1: Datatype is invalid</source>
        <translation>Signal %1 : Attribut datatype invalide</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="328"/>
        <source>No valid datatype</source>
        <translation>Pas de type de données valide</translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="329"/>
        <source>The datatype of &apos;%1&apos; (%2) is not valid! 
 No signals changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="452"/>
        <source>Export C Funktion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/signals.cpp" line="453"/>
        <source>C/C++ File (*.c, *.cpp)</source>
        <translation>Fichier C/C++ (*.c, *.cpp)</translation>
    </message>
</context>
<context>
    <name>SignalsImportDialog</name>
    <message>
        <location filename="../forms/signalsimportdialog.ui" line="14"/>
        <source>Signal Importer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/signalsimportdialog.ui" line="20"/>
        <location filename="../src/signalsimportdialog.cpp" line="30"/>
        <source>Errors:</source>
        <translation>Erreurs :</translation>
    </message>
    <message>
        <location filename="../forms/signalsimportdialog.ui" line="52"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/signalsimportdialog.cpp" line="10"/>
        <source>Successfully imported</source>
        <translation>Import réussi</translation>
    </message>
    <message>
        <location filename="../src/signalsimportdialog.cpp" line="12"/>
        <source>Importing...</source>
        <translation>Import en cours…</translation>
    </message>
</context>
<context>
    <name>TestImportExcel</name>
    <message>
        <location filename="../test/signals/excel/TestImportExcel.cpp" line="69"/>
        <source>Signal %1: Name is invalid</source>
        <translation>Signal %1 : Attribut name invalide</translation>
    </message>
    <message>
        <location filename="../test/signals/excel/TestImportExcel.cpp" line="72"/>
        <location filename="../test/signals/excel/TestImportExcel.cpp" line="75"/>
        <source>Signal %1: Datatype is invalid</source>
        <translation>Signal %1 : Attribut datatype invalide</translation>
    </message>
</context>
<context>
    <name>TestImportXML</name>
    <message>
        <location filename="../test/signals/xml/TestImportXml.cpp" line="59"/>
        <source>Signal %1: Name attribute not found</source>
        <translation>Signal %1 : Attribut name non trouvé</translation>
    </message>
    <message>
        <location filename="../test/signals/xml/TestImportXml.cpp" line="62"/>
        <source>Signal %1: Datatype attribute not found</source>
        <translation>Signal %1 : Attribut datatype non trouvé</translation>
    </message>
    <message>
        <location filename="../test/signals/xml/TestImportXml.cpp" line="65"/>
        <source>Signal %1: Datatype attribute is invalid</source>
        <translation>Signal %1 : Attribut datatype invalide</translation>
    </message>
</context>
<context>
    <name>TriggerWidget</name>
    <message>
        <location filename="../forms/triggerwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="24"/>
        <source>Enable Trigger</source>
        <translation>Activer le déclencheur</translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="31"/>
        <source>Automatic restart of trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="38"/>
        <source>Start the trigger immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="50"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="77"/>
        <source>Value:</source>
        <translation>Valeur :</translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="120"/>
        <source>Triggertype</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="134"/>
        <source>Triggerlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="155"/>
        <source>Time before trigger [s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/triggerwidget.ui" line="179"/>
        <source>Time after trigger [s]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UDP</name>
    <message>
        <location filename="../src/udp.cpp" line="79"/>
        <source>Not able to open UDP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="80"/>
        <source>Hostaddress or Port not valid: &apos;%1&apos;
Maybe a second instance of UDPLogger is open?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="123"/>
        <source>UDP message size greater than max data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="124"/>
        <source>The received UDP message size (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="125"/>
        <source> Byte) is greater than the maximum allowed message size of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="127"/>
        <source>This means, that not every signal can be plotted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="246"/>
        <source>is higher than the UDP buffer size (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/udp.cpp" line="249"/>
        <source>). So the data repeats. Please set the trigger times lower or set the UDP buffersize to a higher value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>changeGraphDialog</name>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="27"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="34"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="61"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="94"/>
        <source>Signalname Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="114"/>
        <source>Signalname X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="140"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="160"/>
        <source>LineStyle</source>
        <translation>Style de ligne</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="180"/>
        <source>ScatterStyle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="211"/>
        <source>y Axis Range Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="217"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="224"/>
        <source>Manual range adjust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="232"/>
        <source>Y min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="250"/>
        <source>Y max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="272"/>
        <source>Automatic range adjustment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="280"/>
        <source>Relati&amp;ve</source>
        <translation>Relatif</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="290"/>
        <source>Absolute</source>
        <translation>Absolu</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="318"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="350"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="357"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../forms/changegraphdialog.ui" line="364"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
</TS>
