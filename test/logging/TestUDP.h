#ifndef TESTUDP_H
#define TESTUDP_H

#include "CommonTest.h"

class TestUDP : public CommonTest {
  Q_OBJECT

private slots:
  void TestRefreshRate();
#ifdef SIMULATE_UDP
  void TestBindFailing();
  void TestDataRateReceived();
  void TestGetData();
  void TestUseData();
#endif

signals:
  void dataChanged(bool);
};

#endif // TESTUDP_H
