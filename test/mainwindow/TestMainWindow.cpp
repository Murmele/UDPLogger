#include "TestMainWindow.h"
#include "mainwindow.h"
#include "plots.h"
#include "triggerwidget.h"

#include <QTemporaryFile>

void TestMainWindow::TestImportExport() {
  QTemporaryFile f;
  QVERIFY(f.open());

  f.fileName();

  // Check that export / import of plots and triggerWidget is included
  {
    MainWindow w;
    QVERIFY(w.m_plots->m_udp_buffer_size != 892384);
    w.m_plots->m_udp_buffer_size = 892384; // random value different to default
    QVERIFY(w.m_triggerwidget->m_settings.type !=
            TriggerType::COUNT_TRIGGER_TYPES);
    w.m_triggerwidget->m_settings.type = TriggerType::COUNT_TRIGGER_TYPES;
    w.exportSettings(f.fileName());
  }
  {
    MainWindow w;
    w.importSettings(f.fileName());

    QCOMPARE(w.m_plots->m_udp_buffer_size, 892384);
    QCOMPARE(w.m_triggerwidget->m_settings.type,
             TriggerType::COUNT_TRIGGER_TYPES);
  }
}

QTEST_MAIN(TestMainWindow);
