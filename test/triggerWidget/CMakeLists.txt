add_executable(TestTriggerWidget TestTriggerWidget.cpp)
target_link_libraries(TestTriggerWidget CommonTest UDPLoggerLib)

# To find ui_triggerwidget
target_include_directories(
  TestTriggerWidget PRIVATE ${CMAKE_BINARY_DIR}/UDPLogger_autogen/include)
set_target_properties(TestTriggerWidget PROPERTIES AUTOUIC_SEARCH_PATHS
                                                   ${CMAKE_SOURCE_DIR}/forms)

add_test(NAME TestTriggerWidget COMMAND TestTriggerWidget)
