#include "TestTriggerWidget.h"
#include "triggerwidget.h"
#include "ui_triggerwidget.h"
#include "signals.h"

#include <QJsonObject>

void TestTriggerWidget::TestImportExport() {

  QJsonObject obj;
  {
    Signals signals_;
    TriggerWidget w(&signals_);

    auto settings = w.settings();

    w.setEnabled(true);
    w.triggerRestartChanged(true);
    w.triggerLevelChanged(-340);
    w.triggerTimeAfterChanged(10);
    w.triggerTimeBeforeChanged(2930);
    w.triggerTypeChanged(TriggerType::COUNT_TRIGGER_TYPES);
    w.signalIndexChanged(5);

    auto settingsNew = w.settings();
    QVERIFY(settingsNew.enabled != settings.enabled);
    QVERIFY(settingsNew.automaticRestart != settings.automaticRestart);
    QVERIFY(settingsNew.level != settings.level);
    QVERIFY(settingsNew.timeAfterTrigger != settings.timeAfterTrigger);
    QVERIFY(settingsNew.timeBeforeTrigger != settings.timeBeforeTrigger);
    QVERIFY(settingsNew.type != settings.type);
    QVERIFY(settingsNew.signalIndex != settings.signalIndex);
    obj["TriggerSettings"] = w.toJsonObject();
  }

  {
    Signals signals_;
    TriggerWidget w(&signals_);
    w.fromJsonObject(obj);
    auto settings = w.settings();

    QCOMPARE(settings.enabled, true);
    QCOMPARE(settings.automaticRestart, true);
    QCOMPARE(settings.level, -340);
    QCOMPARE(settings.timeAfterTrigger, 10);
    QCOMPARE(settings.timeBeforeTrigger, 2930);
    QCOMPARE(settings.type, TriggerType::COUNT_TRIGGER_TYPES);
    QCOMPARE(settings.signalIndex, 5);
  }
}

void TestTriggerWidget::TestSignals() {
  Signals signals_;
  TriggerWidget w(&signals_);

  auto initSettings = w.settings();
  int counter = 0;
  int counterStartTrigger = 0;

  connect(&w, &TriggerWidget::startTrigger,
          [&counterStartTrigger]() { counterStartTrigger++; });

  connect(&w, &TriggerWidget::settingsChanged,
          [initSettings, &counter](const TriggerSettings& settings) {
            counter++;

            auto refSettings = initSettings;

            switch (counter) {
              case 1:
                refSettings.enabled = true;
                QCOMPARE(settings, refSettings);
                break;
              case 2:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                QCOMPARE(settings, refSettings);
                break;
              case 3:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                refSettings.type = TriggerType::FALLING_EDGE;
                QCOMPARE(settings, refSettings);
                break;
              case 4:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                refSettings.type = TriggerType::FALLING_EDGE;
                refSettings.level = 293;
                QCOMPARE(settings, refSettings);
                break;
              case 5:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                refSettings.type = TriggerType::FALLING_EDGE;
                refSettings.level = 293;
                refSettings.timeBeforeTrigger = 238;
                QCOMPARE(settings, refSettings);
                break;
              case 6:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                refSettings.type = TriggerType::FALLING_EDGE;
                refSettings.level = 293;
                refSettings.timeBeforeTrigger = 238;
                refSettings.timeAfterTrigger = 29830;
                QCOMPARE(settings, refSettings);
                break;
              case 7:
                refSettings.enabled = true;
                refSettings.automaticRestart = true;
                refSettings.type = TriggerType::FALLING_EDGE;
                refSettings.level = 293;
                refSettings.timeBeforeTrigger = 238;
                refSettings.timeAfterTrigger = 29830;
                refSettings.signalIndex = 5;
                QCOMPARE(settings, refSettings);
                break;
              default:
                QVERIFY(false);
            }
          });

  auto enable = w.ui->checkbox_enable_trigger;
  emit enable->clicked(Qt::Checked);

  auto restart = w.ui->checkbox_restart_trigger;
  emit restart->clicked(Qt::Checked);

  auto type = w.ui->cb_triggertype;
  emit type->currentIndexChanged(1);

  auto level = w.ui->spinbox_trigger_level;
  emit level->valueChanged(293);

  auto time_before = w.ui->spinbox_t_before_trigger;
  emit time_before->valueChanged(238);

  auto time_after = w.ui->spinbox_t_after_trigger;
  emit time_after->valueChanged(29830);

  auto cbsignals = w.ui->cb_signals;
  emit cbsignals->currentIndexChanged(5);

  QCOMPARE(counterStartTrigger, 0);
  auto start = w.ui->pb_start_trigger;
  emit start->pressed();

  QCOMPARE(counter, 7);
  QCOMPARE(counterStartTrigger, 1);
}

QTEST_MAIN(TestTriggerWidget);
