#ifndef TESTTRIGGERWIDGET_H
#define TESTTRIGGERWIDGET_H

#include "CommonTest.h"

class TestTriggerWidget : public CommonTest {
  Q_OBJECT

private slots:
  void TestImportExport();
  void TestSignals();
};

#endif // TESTTRIGGERWIDGET_H
