#ifndef TESTIMPORTXML_H
#define TESTIMPORTXML_H

#include "CommonTest.h"

class TestImportXML : public CommonTest {
  Q_OBJECT

private slots:
  void Test1();
  void TestInvalidXML();
};

#endif // TESTIMPORTXML_H
