#include "TestSignals.h"
#include "signals.h"

void TestSignals::TestSetSignals() {
  QVector<Signal> refSignals;
  refSignals.append({
      .name = "time",
      .datatype = "int32_t",
      .unit = "ms",
      .struct_name = "",
  });
  refSignals.append({.name = "input_bus.mpu6050data.temp",
                     .datatype = "float",
                     .unit = "°C",
                     .struct_name = "MultikopterInput"});
  refSignals.append({.name = "input_bus.mpu6050data.gyx",
                     .datatype = "float",
                     .unit = "°/s",
                     .struct_name = "MultikopterInput"});
  // At least the name, index and the datatype is needed
  refSignals.append({.name = "input_bus.mpu6050data.gyy",
                     .datatype = "float",
                     .unit = "",
                     .struct_name = ""});

  Signals signals_;
  signals_.setSignals(refSignals);

  QCOMPARE(signals_.getSignalCount(), 4);

  for (int i = 0; i < signals_.getSignalCount(); i++) {
    QCOMPARE(signals_.getSignal(i).index, i);
    QCOMPARE(signals_.getSignal(i).name, refSignals.at(i).name);
    QCOMPARE(signals_.getSignal(i).struct_name, refSignals.at(i).struct_name);
    QCOMPARE(signals_.getSignal(i).unit, refSignals.at(i).unit);
    QCOMPARE(signals_.getSignal(i).datatype, refSignals.at(i).datatype);
    QCOMPARE(signals_.getSignal(i).startByte,
             i * 4); // int32, float, float, float, float
  }
}

QTEST_MAIN(TestSignals);
