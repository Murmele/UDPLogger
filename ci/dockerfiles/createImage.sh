sudo docker build --no-cache -f Dockerfile_Ubuntu -t murmele/udplogger:ubuntu_22.04 .
sudo docker build --no-cache -f Dockerfile_Flatpak -t murmele/udplogger:kde_5.12 .
sudo docker login
sudo docker push murmele/udplogger:ubuntu_22.04
sudo docker push murmele/udplogger:kde_5.12
